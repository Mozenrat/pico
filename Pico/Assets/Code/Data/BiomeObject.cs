using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Code.Data
{
    [CreateAssetMenu]
    public class BiomeObject : ScriptableObject
    {
        public RuleTile BiomeRuleTile;
        public List<Tile> FloorTiles;
        public Tile DefaultWallTile;
        public List<MobAIComponent> BiomeSpawnableMobs;
        public int MaxMobsInitial;
        public int TotalMobsToKillBeforeUnlockPassage;

        public int RoomWidth;
        public int RoomHeight;
        public int RoomGenerationFillPercent;
        public int RoomWallThickness;
    }
}