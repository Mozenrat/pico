using System.Collections.Generic;
using UnityEngine;

namespace Code.Data
{
    public enum DamageablePropType
    {
        WoodenTable,
        WoodenChair,
    }

    public static partial class DataValues
    {
        public const float PlayerMoveSpeed = 300f;
        public const int PlayerMaxHealthInitial = 40;
        public const float PlayerProjectileSpeed = 20f;
        public const int PlayerProjectileTempDamage = 2;
        public const float PlayerProjectileLifetime = .6f;

        public static readonly string RoomPathfindingGraphName = "RoomPathfindingGraph";
        public static readonly float RoomPathfindingNodeSize = .5f;
    
        public static readonly int Animator_PlayerSpeed = Animator.StringToHash("PlayerSpeed");
        public static readonly int Shader_Tint_Property = Shader.PropertyToID("_Tint");
    
        public static readonly int Animator_MobSpeed = Animator.StringToHash("MobSpeed");
        public static readonly int Animator_MobAttack = Animator.StringToHash("MobAttack");
        public static readonly int Animator_MobDamaged = Animator.StringToHash("MobDamaged");

        public static readonly int LayerMaskPlayerAndWallsLayer = LayerMask.GetMask("PlayerCharacter", "Walls");
        public static readonly int LayerMaskWallsLayer = LayerMask.NameToLayer("Walls");
        public static readonly int LayerMaskPlayerLayer = LayerMask.NameToLayer("PlayerCharacter");
        public static readonly int LayerMaskMobLayer = LayerMask.NameToLayer("MobEntity");
        public static readonly int LayerMaskEnvironmentPropsLayer = LayerMask.NameToLayer("EnvironmentProp");

        public static readonly Dictionary<DamageablePropType, int> PropsHealthValues =
            new Dictionary<DamageablePropType, int>
            {
                [DamageablePropType.WoodenTable] = 12,
                [DamageablePropType.WoodenChair] = 4
            };
    }
}