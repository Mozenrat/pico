using UnityEngine;

namespace Code.Data.Mob
{
    [CreateAssetMenu]
    public class MobDataObject : ScriptableObject
    {
        public string MobSpawnStateName;
        
        public float MobMoveSpeed;
        public float MobAttackRange;
        public float MobEngageDistance;
        public float MobAttackCooldown;
        public float MobAttackAnimationDuration;
        public float MobDamagedAnimationDuration;
        
        public float MobProjectileSpeed;
        public float MobProjectileAcceleration;
        public float MobProjectileSpreadLag;
        
        public float MobKnockbackAmount;
        public int MobProjectileDamage;
        public int MobMaxHealth;
    }
}