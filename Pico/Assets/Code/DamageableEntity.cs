using System;
using System.Collections;
using System.Collections.Generic;
using Code.Data;
using UnityEngine;

public class DamageableEntity : MonoBehaviour
{
    [SerializeField] private DamageablePropType PropType;

    private int CurrentHealth;

    private void Awake()
    {
        CurrentHealth = DataValues.PropsHealthValues[PropType];
    }

    public void GetDamaged(int value)
    {
        CurrentHealth -= value;
        if (CurrentHealth <= 0)
        {
            DestroyProp();
        }
    }

    private void DestroyProp()
    {
        //TODO: props destruction system with debris flying around and settling on the ground
        Destroy(gameObject);
    }
}