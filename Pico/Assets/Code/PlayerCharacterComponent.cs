using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.Controllers.RoomGeneration;
using Code.Data;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerCharacterComponent : MonoBehaviour
{
    [HideInInspector] public RoomMap.ConnectionDirection NewRoomSpawnAnchorDirection;
    
    [HideInInspector] public int MaxHealth;
    [HideInInspector] public int CurrentHealth;

    [HideInInspector] public SpriteRenderer PlayerSprite;
    [HideInInspector] public Animator PlayerAnimator;
    [HideInInspector] public Rigidbody2D PlayerRigidbody2d;

    public event Action<int> OnPlayerCurrentHpChanged;

    private MaterialPropertyBlock playerMaterialPropertyBlock;

    private void Awake()
    {
        MaxHealth = DataValues.PlayerMaxHealthInitial;
        CurrentHealth = DataValues.PlayerMaxHealthInitial;
        
        PlayerSprite = GetComponentsInChildren<SpriteRenderer>().Single(item => item.gameObject.name == "Player_Sprite");
        PlayerAnimator = GetComponent<Animator>();
        PlayerRigidbody2d = GetComponent<Rigidbody2D>();

        playerMaterialPropertyBlock = new MaterialPropertyBlock();
    }

    public void TakeDamage(int amount)
    {
        StartCoroutine(TintPlayerRed());
        var dmgNumPos = (Vector2) transform.position + Random.insideUnitCircle * .25f; //TODO: make damage number spawner responsible for pos calc
        ProjectileController.Instance.SpawnDamageNumberAtPosition(amount, dmgNumPos, false);
        CurrentHealth -= amount;
        OnPlayerCurrentHpChanged?.Invoke(CurrentHealth);
    }

    private IEnumerator TintPlayerRed()
    {
        PlayerSprite.GetPropertyBlock(playerMaterialPropertyBlock);
        playerMaterialPropertyBlock.SetColor(DataValues.Shader_Tint_Property, Color.red);
        PlayerSprite.SetPropertyBlock(playerMaterialPropertyBlock);

        var tintDuration = 0.1f;
        while (tintDuration > 0)
        {
            tintDuration -= Time.deltaTime;
            yield return null;
        }
        
        PlayerSprite.GetPropertyBlock(playerMaterialPropertyBlock);
        playerMaterialPropertyBlock.SetColor(DataValues.Shader_Tint_Property, Color.black);
        PlayerSprite.SetPropertyBlock(playerMaterialPropertyBlock);
    }
}