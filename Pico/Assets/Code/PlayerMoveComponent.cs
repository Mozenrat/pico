using System;
using System.Linq;
using Code.Data;
using UnityEngine;

namespace Code
{
    public class PlayerMoveComponent : MonoBehaviour
    {
        private PlayerCharacterComponent playerCharComponent;
        private PlayerInputHandler playerInputHandler;

        private void Awake()
        {
            playerCharComponent = GetComponent<PlayerCharacterComponent>();
            playerInputHandler = FindObjectOfType<PlayerInputHandler>();
        }

        private void OnPlayerInput(object sender, PlayerInputHandler.PlayerInputEventArgs eventArgs)
        {
            var positionTarget = eventArgs.currentMoveDelta * (Time.fixedDeltaTime * DataValues.PlayerMoveSpeed);
            playerCharComponent.PlayerRigidbody2d.velocity = positionTarget;
        
            playerCharComponent.PlayerAnimator.SetFloat(DataValues.Animator_PlayerSpeed, eventArgs.currentMoveDelta.magnitude);

            var playerX = transform.position.x;
            if (eventArgs.currentCursorPos.x > playerX)
            {
                playerCharComponent.PlayerSprite.flipX = false;
            }
            else if (eventArgs.currentCursorPos.x < playerX)
            {
                playerCharComponent.PlayerSprite.flipX = true;
            }
        }

        private void OnEnable()
        {
            playerInputHandler.OnPlayerInput += OnPlayerInput;
        }

        private void OnDisable()
        {
            playerInputHandler.OnPlayerInput -= OnPlayerInput;
        }
    }
}