using System;
using System.Collections;
using System.Collections.Generic;
using Code.Data;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    [SerializeField] private ParticleSystem PlayerProjectileCollisionParticlePrefab;
    private Vector2 flightDirection;
    private float playerProjectileLifeCurrent;

    private void Start()
    {
        flightDirection = transform.position - GlobalGameState.Instance.PlayerCharacterRef.transform.position;
    }
    
    private void Update()
    {
        playerProjectileLifeCurrent += Time.deltaTime;
        if (playerProjectileLifeCurrent >= DataValues.PlayerProjectileLifetime)
        {
            Destroy(gameObject);
            return;
        }

        transform.position += (Vector3)flightDirection.normalized * (DataValues.PlayerProjectileSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var otherLayer = other.gameObject.layer;
        
        if (otherLayer == DataValues.LayerMaskMobLayer)
        {
            var directionForKnockback = transform.position - GlobalGameState.Instance.PlayerCharacterRef.transform.position;
            var mobAi = other.GetComponent<MobAIComponent>();
            mobAi.TakeDamage(DataValues.PlayerProjectileTempDamage, directionForKnockback.normalized);
        }
        if (otherLayer == DataValues.LayerMaskEnvironmentPropsLayer)
        {
            var damageable = other.GetComponent<DamageableEntity>();
            damageable.GetDamaged(DataValues.PlayerProjectileTempDamage);
        }

        Instantiate(PlayerProjectileCollisionParticlePrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}