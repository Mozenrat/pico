using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class ProjectilePooler
{
    private GameObject poolsParent;
    private Dictionary<ProjectileType, Queue<MobProjectile>> projectilePools;
    private Queue<FloatingDamageComponent> damageNumbersPool;
    
    private int poolsCapacity = 50;
    
    public ProjectilePooler()//TODO: add params for projPrefabs, assigned in controller or scriptable objects
    {
        projectilePools = new Dictionary<ProjectileType, Queue<MobProjectile>>();
        projectilePools.Add(ProjectileType.SkeletonBone, new Queue<MobProjectile>());

        poolsParent = new GameObject("Projectile_Pools");
        for (int i = 0; i < poolsCapacity; i++)
        {
            var newGameObject = Object.Instantiate(ProjectileController.Instance.SkeletonProjectilePrefab,
                Vector3.zero,
                Quaternion.identity,
                poolsParent.transform);
            newGameObject.gameObject.SetActive(false);
            projectilePools[ProjectileType.SkeletonBone].Enqueue(newGameObject);
        }

        damageNumbersPool = new Queue<FloatingDamageComponent>();
        for (int i = 0; i < poolsCapacity; i++)
        {
            var newGameObject = Object.Instantiate(ProjectileController.Instance.FloatingDamagePrefab,
                Vector3.zero,
                Quaternion.identity,
                poolsParent.transform);
            newGameObject.gameObject.SetActive(false);
            damageNumbersPool.Enqueue(newGameObject);
        }
    }

    public MobProjectile GetProjectileFromPool(ProjectileType projectileType)
    {
        var projectile = projectilePools[projectileType].Dequeue();
        return projectile;
    }

    public void ReturnProjectileToPool(ProjectileType projectileType, MobProjectile projectileGameObject)
    {
        projectilePools[projectileType].Enqueue(projectileGameObject);
    }

    public FloatingDamageComponent GetDamageNumberFromPool()
    {
        return damageNumbersPool.Dequeue();
    }

    public void ReturnDamageNumberToPool(FloatingDamageComponent floatingDamageComponent)
    {
        damageNumbersPool.Enqueue(floatingDamageComponent);
    }
}