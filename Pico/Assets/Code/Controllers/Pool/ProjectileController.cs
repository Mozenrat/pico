using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public static ProjectileController Instance;

    public FloatingDamageComponent FloatingDamagePrefab;
    
    public MobProjectile SkeletonProjectilePrefab; // TODO: temp
    
    private ProjectilePooler projectilePooler;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(gameObject);
        
        projectilePooler = new ProjectilePooler();
    }

    public MobProjectile SpawnProjectileAtPosition(ProjectileType projectileType, Vector3 pos, Quaternion rot)
    {
        var proj = projectilePooler.GetProjectileFromPool(projectileType);
        proj.gameObject.SetActive(true);
        proj.transform.SetPositionAndRotation(pos, rot);
        return proj;
    }

    public void ReturnProjectileToPool(MobProjectile projObject)
    {
        projObject.gameObject.SetActive(false);
        projectilePooler.ReturnProjectileToPool(projObject.ProjectileType, projObject);
    }

    public void SpawnDamageNumberAtPosition(int damageValue, Vector3 pos, bool isEnemyDamaged)
    {
        var damageObject = projectilePooler.GetDamageNumberFromPool();
        damageObject.gameObject.SetActive(true);
        damageObject.transform.SetPositionAndRotation(pos, Quaternion.identity);
        damageObject.InitFloatingDamage(damageValue, isEnemyDamaged);
    }

    public void ReturnDamageNumberToPool(FloatingDamageComponent damageComponent)
    {
        damageComponent.gameObject.SetActive(false);
        projectilePooler.ReturnDamageNumberToPool(damageComponent);
    }
}