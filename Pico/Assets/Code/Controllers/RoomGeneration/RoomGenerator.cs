using System;
using System.Collections;
using System.Collections.Generic;
using Code.Data;
using Pathfinding;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Code.Controllers.RoomGeneration
{
    public class RoomGenerator : MonoBehaviour
    {
        [SerializeField] private SceneSwitchComponent SceneSwitchPrefab;

        private Vector2Int originPoint = new Vector2Int(0, 0);

        public RoomMap GenerateRoom(BiomeObject biomeObject,
            Tilemap floorTilemap,
            Tilemap wallTilemap,
            RoomMap.ConnectionDirection playerInDirection,
            out List<SceneSwitchComponent> sceneSwitches)
        {
            //TODO: Need to calculate this from "overworld map or smth"
            List<RoomMap.ConnectionDirection> connectionDirections = new List<RoomMap.ConnectionDirection>
            {
                RoomMap.ConnectionDirection.Up,
                RoomMap.ConnectionDirection.Right,
                RoomMap.ConnectionDirection.Down,
                RoomMap.ConnectionDirection.Left
            };
            RoomMap room = new RoomMap(
                biomeObject.RoomWidth,
                biomeObject.RoomHeight,
                biomeObject.RoomGenerationFillPercent,
                biomeObject.RoomWallThickness,
                originPoint,
                connectionDirections);

            room.RenderTilemap(wallTilemap, floorTilemap, biomeObject.BiomeRuleTile, biomeObject.DefaultWallTile, biomeObject.FloorTiles);

            StartCoroutine(GeneratePathfindingGraph(biomeObject));

            sceneSwitches = GenerateRoomSwitchObjects(playerInDirection, room, floorTilemap);

            return room;
        }

        private IEnumerator GeneratePathfindingGraph(BiomeObject biomeObject)
        {
            var graph = AstarPath.active.data.FindGraph(item => item.name == DataValues.RoomPathfindingGraphName);
            if (graph != null) AstarPath.active.data.RemoveGraph(graph);
        
            yield return new WaitForEndOfFrame();//Need to do this to let tilemaps properly update first
        
            AstarData astarData = AstarPath.active.data;
        
            var gridGraph = astarData.FindGraph(item => item.name == DataValues.RoomPathfindingGraphName) as GridGraph;
            if (gridGraph == null)
            {
                gridGraph = astarData.AddGraph(typeof(GridGraph)) as GridGraph;
            }

            gridGraph.name = DataValues.RoomPathfindingGraphName;
            gridGraph.center = new Vector3(originPoint.x + biomeObject.RoomWidth/2f, originPoint.y + biomeObject.RoomHeight/2f, 0);
            gridGraph.rotation = new Vector3(-90f, 0f, 0f);
            gridGraph.SetDimensions(biomeObject.RoomWidth * 2, biomeObject.RoomHeight * 2, DataValues.RoomPathfindingNodeSize);
            gridGraph.collision.use2D = true;
            gridGraph.collision.mask = LayerMask.GetMask("Walls");
        
            AstarPath.active.Scan(gridGraph);
        }
    
        private List<SceneSwitchComponent> GenerateRoomSwitchObjects(RoomMap.ConnectionDirection playerInDirection, RoomMap room, Tilemap floorTilemap)
        {
            List<SceneSwitchComponent> sceneSwitches = new List<SceneSwitchComponent>();
            var passagePositions = room.GetConnectionPassagePositions(floorTilemap);
            foreach (var passageAnchor in passagePositions)
            {
                if (passageAnchor.PassageDirection == playerInDirection)
                {
                    Vector3 offsetDirection = playerInDirection switch
                    {
                        RoomMap.ConnectionDirection.Up => Vector3.down,
                        RoomMap.ConnectionDirection.Right => Vector3.left,
                        RoomMap.ConnectionDirection.Down => Vector3.up,
                        RoomMap.ConnectionDirection.Left => Vector3.right,
                        _ => throw new ArgumentOutOfRangeException(nameof(playerInDirection), playerInDirection, null)
                    };
                    GlobalGameState.Instance.PlayerCharacterRef.transform.position =
                        passageAnchor.WorldPosition + offsetDirection * 4f;
                }

                Quaternion rotation;
                if (passageAnchor.PassageDirection == RoomMap.ConnectionDirection.Up ||
                    passageAnchor.PassageDirection == RoomMap.ConnectionDirection.Down)
                {
                    rotation = quaternion.identity;
                }
                else
                {
                    rotation = Quaternion.Euler(0f, 0f, 90f);
                }

                var sceneSwitch = Instantiate(SceneSwitchPrefab,
                    passageAnchor.WorldPosition,
                    rotation,
                    gameObject.transform);
                sceneSwitch.name = passageAnchor.PassageDirection.ToString();
                sceneSwitches.Add(sceneSwitch);
            }

            return sceneSwitches;
        }
    }
}