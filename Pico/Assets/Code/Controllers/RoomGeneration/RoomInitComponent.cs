using System.Collections.Generic;
using Cinemachine;
using Code.Data;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Code.Controllers.RoomGeneration
{
    public class RoomInitComponent : MonoBehaviour
    {
        [SerializeField] private BiomeObject BiomeObject;
        private RoomGenerator roomGeneratorComponent;
        private RoomMobSpawner mobSpawner;
    
        [SerializeField] private CinemachineVirtualCamera SceneCamera;
        [SerializeField] private PolygonCollider2D CameraConfinerCollider;

        [SerializeField] private Tilemap WallsTilemap;
        [SerializeField] private Tilemap FloorsTilemap;

        private void Awake()
        {
            roomGeneratorComponent = GetComponent<RoomGenerator>();
            mobSpawner = new RoomMobSpawner();
        }

        private void Start()
        {
            FloorsTilemap.ClearAllTiles();
            WallsTilemap.ClearAllTiles();
            
            var generatedRoom = roomGeneratorComponent.GenerateRoom(BiomeObject,
                FloorsTilemap,
                WallsTilemap,
                GlobalGameState.Instance.PlayerCharacterRef.NewRoomSpawnAnchorDirection,
                out var sceneSwitchComponents);
            
            foreach (var sceneSwitch in sceneSwitchComponents)
            {
                sceneSwitch.SetColliderTriggerState(false);
            }
            
            mobSpawner.InitMobSpawningInRoom(generatedRoom, FloorsTilemap, BiomeObject, sceneSwitchComponents);

            SetupRoomCamera();
            generatedRoom.DebugVisualizeConnectionLinesOnTilemap(FloorsTilemap);

            var wtf2 = generatedRoom.GeneratePoissonPoints();
            foreach (var point in wtf2)
            {
                Debug.DrawRay(point, Vector3.up * .25f, Color.magenta, 10f);
            }
            
            var wtf = generatedRoom.GetFloorSegmentsWithSize(4, 3, FloorsTilemap);
            foreach (var point in wtf)
            {
                for (int i = 0; i < 4; i++)
                for (int j = 0; j < 3; j++)
                {
                    //Debug.DrawRay(point + new Vector3(i,j,0f), Vector3.up * .25f, Color.magenta, 10f);
                }
            }
        }

        private void SetupRoomCamera()
        {
            CameraConfinerCollider.SetPath(0, new []
            {
                new Vector2(1, BiomeObject.RoomHeight - 1),
                new Vector2(BiomeObject.RoomWidth - 1, BiomeObject.RoomHeight - 1),
                new Vector2(BiomeObject.RoomWidth - 1, 1),
                new Vector2(1, 1)
            });
            var cameraConfiner = SceneCamera.GetComponent<CinemachineConfiner2D>();
            cameraConfiner.InvalidateCache();
            cameraConfiner.m_BoundingShape2D = CameraConfinerCollider;

            SceneCamera.ForceCameraPosition(GlobalGameState.Instance.PlayerCharacterRef.transform.position,
                Quaternion.identity);
            SceneCamera.Follow = GlobalGameState.Instance.PlayerCharacterRef.transform;
        }
    }
}