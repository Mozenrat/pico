﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace Code.Controllers.RoomGeneration
{
    public class RoomMap
    {
        public enum ConnectionDirection
        {
            Up,
            Right,
            Down,
            Left
        }

        public class ConnectionPassageAnchor
        {
            public Vector2Int CellPosition;
            public Vector3 WorldPosition;
            public ConnectionDirection PassageDirection;
        }

        public readonly List<RoomRegion> AllInitialFloors;
        public List<Vector2Int> FinalFloors;
        public List<Vector2Int> FinalWalls;
        
        private readonly List<ConnectionPassageAnchor> connectionPassageAnchors;
        private int smoothIterations = 6;
        private int maxConnectionPassageLength = 25;
        private int connectionPassageThickness = 5;

        private Vector2Int mapOrigin;

        private readonly int width;
        private readonly int height;

        private readonly IntegerMap integerMap;

        private readonly PoissonSampler poissonSampler;

        public RoomMap(int mapWidth, int mapHeight, int fillPercent, int borderWidth, Vector2Int originPoint, List<ConnectionDirection> connectionDirections)
        {
            poissonSampler = new PoissonSampler();
            mapOrigin = originPoint;

            width = mapWidth;
            height = mapHeight;

            integerMap = new IntegerMap(width, height, fillPercent, borderWidth);

            AllInitialFloors = GetRegionsOfType(0);

            foreach (var floorRegion in AllInitialFloors)
            {
                floorRegion.CalculateClosestRegion(AllInitialFloors);
            }

            foreach (var floorRegion in AllInitialFloors)
            {
                floorRegion.ConnectToNearestRegion(integerMap);
            }

            connectionPassageAnchors = new List<ConnectionPassageAnchor>();
            foreach (var side in connectionDirections)
            {
                var centerClosestToSide = FindRegionCenterClosestToSide(side);
                var anchor = CalculateConnectionAnchorToRegionCenter(side, centerClosestToSide);
                connectionPassageAnchors.Add(new ConnectionPassageAnchor
                {
                    CellPosition = anchor,
                    PassageDirection = side
                });
                CutOutConnectionPassage(anchor, side);
            }

            DecimateLooseWalls();
            PopulateFinalCellValues();
        }
        
        public void RenderTilemap(Tilemap wallsTilemap, Tilemap floorsTilemap, RuleTile ruleTile, Tile regularWallTile, List<Tile> floorTiles)
        {
            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
            {
                Vector3Int tilePos = new Vector3Int(mapOrigin.x + x, mapOrigin.y + y, 0);
                if (integerMap[x, y] == 1)
                {
                    wallsTilemap.SetTile(tilePos, ruleTile);
                }
                else
                {
                    floorsTilemap.SetTile(tilePos, floorTiles[Random.Range(0, floorTiles.Count)]);
                }
            }
            
            var fillWithWall = GetAndSetAllTopSideTiles();
            foreach (var tile in fillWithWall)
            {
                Vector3Int tilePos = new Vector3Int(mapOrigin.x + tile.x, mapOrigin.y + tile.y, 0);
                wallsTilemap.SetTile(tilePos, regularWallTile);
            }
        }

        public List<Vector3> GeneratePoissonPoints()
        {
            return poissonSampler.GeneratePoints(new Vector2(width, height), integerMap);
        }

        /// <summary>
        /// Get a floor segment with provided dimensions
        /// </summary>
        /// <param name="segmentWidth">region width</param>
        /// <param name="segmentHeight">region height</param>
        /// <param name="floorTilemap">tilemap for floors to calculate world position</param>
        /// <returns>List of segment anchors, which are world positions of cell center of left-bottom tile</returns>
        public List<Vector3> GetFloorSegmentsWithSize(int segmentWidth, int segmentHeight, Tilemap floorTilemap)
        {
            List<Vector3> segmentAnchors = new List<Vector3>();
            
            foreach (var roomRegion in AllInitialFloors)
            {
                var regionCenterCell = roomRegion.RegionBoundsCenter;
                
                bool shouldDiscardRegion = false;
                var leftBottomPoint = new Vector2Int(regionCenterCell.x - Mathf.FloorToInt(segmentHeight / 2f),
                    regionCenterCell.y - Mathf.FloorToInt(segmentHeight / 2f));
                for (int x = leftBottomPoint.x; x < leftBottomPoint.x + segmentWidth; x++)
                {
                    if (shouldDiscardRegion) break;
                    for (int y = leftBottomPoint.y; y < leftBottomPoint.y + segmentHeight; y++)
                    {
                        //Debug.DrawRay(GetCellPositionWorld(new Vector2Int(x, y), floorTilemap), Vector3.up*.2f, Color.green, 10f);
                        if (integerMap[x, y] == 1)
                        {
                            shouldDiscardRegion = true;
                            break;
                        }
                    }
                }

                if (!shouldDiscardRegion)
                {
                    segmentAnchors.Add(GetCellPositionWorld(leftBottomPoint, floorTilemap));
                }
            }
            
            return segmentAnchors;
        }

        public List<Vector3> GetRandomFloorCells(int amount, Tilemap floorTilemap)
        {
            var cellsList = new List<Vector3>();
            int[] randomCells = new int[amount];

            for (int i = 0; i < amount; i++)
            {
                var rngIndex = Random.Range(0, FinalFloors.Count);

                if (!randomCells.Contains(rngIndex))
                {
                    cellsList.Add(GetCellPositionWorld(FinalFloors[rngIndex], floorTilemap));
                    randomCells[i] = rngIndex;
                }
                else
                {
                    i--;
                }
            }
            
            return cellsList;
        }

        public List<ConnectionPassageAnchor> GetConnectionPassagePositions(Tilemap floorTilemap)
        {
            foreach (var anchor in connectionPassageAnchors)
            {
                anchor.WorldPosition = GetCellPositionWorld(anchor.CellPosition, floorTilemap);
            }

            return connectionPassageAnchors;
        }

        public void DebugVisualizePerimetersOnTilemap(Tilemap tilemap)
        {
            foreach (var region in AllInitialFloors)
            foreach (var perimeterTile in region.RegionPerimeterTiles)
            {
                var tilePoint = new Vector3Int(perimeterTile.x + mapOrigin.x, perimeterTile.y + mapOrigin.y, 0);
                var worldPoint = tilemap.GetCellCenterWorld(tilePoint);
                Debug.DrawRay(worldPoint, Vector3.up * .2f, Color.blue, 5f);
            }
        }

        public void DebugVisualizeConnectionLinesOnTilemap(Tilemap tilemap)
        {
            foreach (var roomRegion in AllInitialFloors)
            {
                if (roomRegion.NearestSimilarRegion == null) continue;

                var regionCenter = GetCellPositionWorld(roomRegion.RegionBoundsCenter, tilemap);
                var nearestRegionCenter = GetCellPositionWorld(roomRegion.NearestSimilarRegion.RegionBoundsCenter, tilemap);
                Debug.DrawLine(regionCenter, nearestRegionCenter, Color.cyan, 5f);
            }
        }

        private void PopulateFinalCellValues()
        {
            FinalFloors = new List<Vector2Int>();
            FinalWalls = new List<Vector2Int>();
            
            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
            {
                if (integerMap[x, y] == 0)
                {
                    FinalFloors.Add(new Vector2Int(x, y));
                }
                else
                {
                    FinalWalls.Add(new Vector2Int(x, y));
                }
            }
        }

        private void CutOutConnectionPassage(Vector2Int connectionAnchor, ConnectionDirection direction)
        {
            Vector2Int connectionOriginPoint;
            Vector2Int thicknessIncrement;
            Vector2Int passageGrowIncrement;
            switch (direction)
            {
                case ConnectionDirection.Up:
                    connectionOriginPoint = new Vector2Int(connectionAnchor.x - Mathf.RoundToInt(connectionPassageThickness / 2f), connectionAnchor.y);
                    thicknessIncrement = Vector2Int.right;
                    passageGrowIncrement = Vector2Int.down;
                    break;
                case ConnectionDirection.Right:
                    connectionOriginPoint = new Vector2Int(connectionAnchor.x, connectionAnchor.y - Mathf.RoundToInt(connectionPassageThickness / 2f));
                    thicknessIncrement = Vector2Int.up;
                    passageGrowIncrement = Vector2Int.left;
                    break;
                case ConnectionDirection.Down:
                    connectionOriginPoint = new Vector2Int(connectionAnchor.x - Mathf.RoundToInt(connectionPassageThickness / 2f), connectionAnchor.y);
                    thicknessIncrement = Vector2Int.right;
                    passageGrowIncrement = Vector2Int.up;
                    break;
                case ConnectionDirection.Left:
                    connectionOriginPoint = new Vector2Int(connectionAnchor.x, connectionAnchor.y - Mathf.RoundToInt(connectionPassageThickness / 2f));
                    thicknessIncrement = Vector2Int.up;
                    passageGrowIncrement = Vector2Int.right;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            for (int i = 0; i < connectionPassageThickness; i++)
            {
                Vector2Int currentPoint = connectionOriginPoint + thicknessIncrement * i;
                int cellsTraversed = 0;
                while (cellsTraversed < maxConnectionPassageLength)
                {
                    cellsTraversed++;
                    integerMap[currentPoint.x, currentPoint.y] = 0;

                    var nextCell = currentPoint + passageGrowIncrement;
                    if (integerMap[nextCell.x, nextCell.y] == 0 )
                    {
                        break;
                    }

                    currentPoint += passageGrowIncrement;
                }
            }
        }

        private Vector2Int CalculateConnectionAnchorToRegionCenter(ConnectionDirection direction, Vector2Int regionCenter)
        {
            switch (direction)
            {
                case ConnectionDirection.Up:
                    return new Vector2Int(regionCenter.x, height - 1);
                case ConnectionDirection.Right:
                    return new Vector2Int(width - 1, regionCenter.y);
                case ConnectionDirection.Down:
                    return new Vector2Int(regionCenter.x, 0);
                case ConnectionDirection.Left:
                    return new Vector2Int(0, regionCenter.y);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        private Vector2Int FindRegionCenterClosestToSide(ConnectionDirection side)
        {
            Vector2Int sideCenterPoint;
            switch (side)
            {
                case ConnectionDirection.Up:
                    sideCenterPoint = new Vector2Int(Mathf.RoundToInt(width / 2f), height - 1);
                    break;
                case ConnectionDirection.Right:
                    sideCenterPoint = new Vector2Int(width - 1, Mathf.RoundToInt(height / 2f));
                    break;
                case ConnectionDirection.Down:
                    sideCenterPoint = new Vector2Int(Mathf.RoundToInt(width / 2f), 0);
                    break;
                case ConnectionDirection.Left:
                    sideCenterPoint = new Vector2Int(0, Mathf.RoundToInt(height / 2f));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(side));
            }

            Vector2Int closestRegionCenter = Vector2Int.zero;
            float minDistance = float.MaxValue;

            foreach (var region in AllInitialFloors)
            {
                var currentDistance = Vector2Int.Distance(region.RegionBoundsCenter, sideCenterPoint);
                if (currentDistance < minDistance)
                {
                    closestRegionCenter = region.RegionBoundsCenter;
                    minDistance = currentDistance;
                }
            }

            return closestRegionCenter;
        }

        private Vector3 GetCellPositionWorld(Vector2Int regionPoint, Tilemap tilemap)
        {
            if (!integerMap.IsWithinIntegerMapBounds(regionPoint.x, regionPoint.y))
                throw new ArgumentOutOfRangeException(nameof(regionPoint));

            var nearestTilePoint = new Vector3Int(regionPoint.x + mapOrigin.x, regionPoint.y + mapOrigin.y, 0);
            var nearestWorldPoint = tilemap.GetCellCenterWorld(nearestTilePoint);
            return nearestWorldPoint;
        }

        private List<RoomRegion> GetRegionsOfType(int tileType)
        {
            List<RoomRegion> allRegions = new List<RoomRegion>();
            bool[,] visitedCells = new bool[width, height];

            for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                if (!visitedCells[x, y] && integerMap[x, y] == tileType)
                {
                    var floodFilled = GetFloodFilledArea(x, y, tileType);
                    var newRegion = new RoomRegion(this, floodFilled, tileType, integerMap);
                    allRegions.Add(newRegion);

                    foreach (var tilePos in newRegion.RegionTiles)
                    {
                        visitedCells[tilePos.x, tilePos.y] = true;
                    }
                }
            }

            return allRegions;
        }

        private List<Vector2Int> GetFloodFilledArea(int xStart, int yStart, int tileType)
        {
            List<Vector2Int> area = new List<Vector2Int>();
            bool[,] visitedCells = new bool[width, height];

            Queue<Vector2Int> floodFillQueue = new Queue<Vector2Int>();
            floodFillQueue.Enqueue(new Vector2Int(xStart, yStart));
            visitedCells[xStart, yStart] = true;

            while (floodFillQueue.Count > 0)
            {
                var currentCell = floodFillQueue.Dequeue();
                area.Add(currentCell);

                for (int x = currentCell.x - 1; x <= currentCell.x + 1; x++)
                for (int y = currentCell.y - 1; y <= currentCell.y + 1; y++)
                {
                    if (integerMap.IsWithinIntegerMapBounds(x, y) && (x == currentCell.x || y == currentCell.y))
                    {
                        if (!visitedCells[x, y] && integerMap[x, y] == tileType)
                        {
                            visitedCells[x, y] = true;
                            floodFillQueue.Enqueue(new Vector2Int(x, y));
                        }
                    }
                }
            }

            return area;
        }

        private List<Vector2Int> GetAndSetAllTopSideTiles()
        {
            List<Vector2Int> tiles = new List<Vector2Int>();

            for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                if (!integerMap.IsWithinIntegerMapBounds(x, y - 1)) continue;

                if (integerMap[x, y] == 1 && integerMap[x, y - 1] == 0)
                {
                    integerMap[x, y - 1] = 1;
                    tiles.Add(new Vector2Int(x, y - 1));
                }
            }

            return tiles;
        }

        private void DecimateLooseWalls()
        {
            //decimate single-walls left after making corridors
            for (int i = 0; i < smoothIterations * 2; i++)
            {
                for (int x = 0; x < width; x++)
                for (int y = 0; y < height; y++)
                {
                    int neighbourAmount = integerMap.GetAdjacentWallCount(x, y);

                    if (neighbourAmount <= 3)
                        integerMap[x, y] = 0;
                }
            }

            //decimate vertical doubles, they break tiling
            for (int x = 0; x < width; x++)
            for (int y = 0; y < height; y++)
            {
                if (integerMap.IsWithinIntegerMapBounds(x, y - 2) && integerMap.IsWithinIntegerMapBounds(x, y + 1))
                {
                    if (integerMap[x, y] == 1 &&
                        integerMap[x, y - 1] == 1 &&
                        integerMap[x, y + 1] == 0 &&
                        integerMap[x, y - 2] == 0)
                    {
                        integerMap[x, y] = 0;
                        integerMap[x, y - 1] = 0;
                    }
                }
            }
        }
    }
}