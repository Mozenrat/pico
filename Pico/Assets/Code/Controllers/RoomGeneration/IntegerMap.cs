﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Controllers.RoomGeneration
{
    public class IntegerMap
    {
        private int[,] mapArrays;
        private int mapWidth;
        private int mapHeight;
        
        private int smoothIterations = 6;
        private int growTileAdjacentCount = 4; //TODO: this should not be here
        private int decimateTileAdjacentCount = 4;

        public int this[int x, int y] 
        {
            get => mapArrays[x,y];
            set => mapArrays[x, y] = value;
        }

        public int this[Vector2Int vector2Int]
        {
            get => mapArrays[vector2Int.x, vector2Int.y];
            set => mapArrays[vector2Int.x, vector2Int.y] = value;
        }

        public IntegerMap(int width, int height, int fillPercent, int borderThickness)
        {
            mapArrays = new int[width, height];
            mapWidth = width;
            mapHeight = height;
            
            GenerateIntMap(fillPercent, borderThickness);
            for (int i = 0; i < smoothIterations; i++)
            {
                SmoothIntMap();
            }
        }
        
        public bool IsWithinIntegerMapBounds(int x, int y)
        {
            return x >= 0 && x < mapWidth && y >= 0 && y < mapHeight;
        }
        
        public int GetAdjacentWallCount(int xPos, int yPos)
        {
            int wallCount = 0;
            for (int x = xPos - 1; x <= xPos + 1; x++)
            for (int y = yPos - 1; y <= yPos + 1; y++)
            {
                if (IsWithinIntegerMapBounds(x, y))
                {
                    if (x != xPos || y != yPos)
                        wallCount += mapArrays[x, y];
                }
                else
                {
                    wallCount++;
                }
            }

            return wallCount;
        }

        public List<Vector2Int> GetAllAdjacentIntegerPoints(Vector3 point)
        {
            List<Vector2Int> pointsList = new List<Vector2Int>();

            int xPos = Mathf.FloorToInt(point.x);
            int yPos = Mathf.FloorToInt(point.y);
            
            for (int x = xPos - 1; x <= xPos + 1; x++)
            for (int y = yPos - 1; y <= yPos + 1; y++)
            {
                if (IsWithinIntegerMapBounds(x, y))
                {
                    pointsList.Add(new Vector2Int(x, y));
                }
            }

            return pointsList;
        }

        private void GenerateIntMap(int fillPercent, int borderThickness)
        {
            for (var x = 0; x < mapWidth; x++)
            for (var y = 0; y < mapHeight; y++)
            {
                if (x < (borderThickness + 1) ||
                    x > mapWidth - (borderThickness + 1) ||
                    y < (borderThickness + 1) ||
                    y > mapHeight - (borderThickness + 1)) // set all border tiles to 1
                {
                    mapArrays[x, y] = 1;
                }
                else
                {
                    mapArrays[x, y] = Random.Range(0, 100) < fillPercent ? 1 : 0;
                }
            }
        }
        
        private void SmoothIntMap()
        {
            for (int x = mapWidth - 1; x >= 0; x--)
            for (int y = mapHeight - 1; y >= 0; y--)
            {
                int neighbourAmount = GetAdjacentWallCount(x, y);

                if (neighbourAmount > growTileAdjacentCount)
                    mapArrays[x, y] = 1;
                else if (neighbourAmount < decimateTileAdjacentCount)
                    mapArrays[x, y] = 0;
            }
        }
    }
}