﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Controllers.RoomGeneration
{
    public class PoissonSampler
    {
        public List<Vector3> GeneratePoints(Vector2 pointsSampleRegion, IntegerMap integerMap)
        {
            var radius = 2.5f;
            float cellSize = radius/Mathf.Sqrt(2);
            var numSamplesBeforeRejection = 20;
            
            int[,] localGrid = new int[Mathf.CeilToInt(pointsSampleRegion.x/cellSize), Mathf.CeilToInt(pointsSampleRegion.y/cellSize)];
            List<Vector3> pointsList = new List<Vector3>();
            List<Vector2> spawnPoints = new List<Vector2>();

            spawnPoints.Add(pointsSampleRegion/2);
            while (spawnPoints.Count > 0) 
            {
                int spawnIndex = Random.Range(0,spawnPoints.Count);
                Vector2 spawnCentre = spawnPoints[spawnIndex];
                bool candidateAccepted = false;

                for (int i = 0; i < numSamplesBeforeRejection; i++)
                {
                    float angle = Random.value * Mathf.PI * 2;
                    Vector2 dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
                    Vector2 candidate = spawnCentre + dir * Random.Range(radius, 2*radius);
                    if (IsValid(candidate, pointsSampleRegion, cellSize, radius, pointsList, localGrid)) 
                    {
                        pointsList.Add(candidate);
                        spawnPoints.Add(candidate);
                        localGrid[(int)(candidate.x/cellSize),(int)(candidate.y/cellSize)] = pointsList.Count;
                        candidateAccepted = true;
                        break;
                    }
                }
                if (!candidateAccepted) 
                {
                    spawnPoints.RemoveAt(spawnIndex);
                }

            }
            
            ProcessPointsBounds(pointsList, integerMap);

            return pointsList;
        }
        
        private bool IsValid(Vector2 candidate,
            Vector2 sampleRegionSize,
            float cellSize,
            float radius,
            List<Vector3> points,
            int[,] localGrid) 
        {
            if (candidate.x >=0 && candidate.x < sampleRegionSize.x && candidate.y >= 0 && candidate.y < sampleRegionSize.y) 
            {
                int cellX = (int)(candidate.x/cellSize);
                int cellY = (int)(candidate.y/cellSize);
                int searchStartX = Mathf.Max(0,cellX -2);
                int searchEndX = Mathf.Min(cellX+2,localGrid.GetLength(0)-1);
                int searchStartY = Mathf.Max(0,cellY -2);
                int searchEndY = Mathf.Min(cellY+2,localGrid.GetLength(1)-1);

                for (int x = searchStartX; x <= searchEndX; x++) 
                {
                    for (int y = searchStartY; y <= searchEndY; y++)
                    {
                        int pointIndex = localGrid[x,y]-1;
                        if (pointIndex != -1) 
                        {
                            float sqrDst = (candidate - (Vector2)points[pointIndex]).sqrMagnitude;
                            if (sqrDst < radius*radius) 
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void ProcessPointsBounds(List<Vector3> points, IntegerMap roomIntegerMap)
        {
            for (var i = points.Count - 1; i >= 0; i--)
            {
                var point = points[i];
                var adjacentPoints = roomIntegerMap.GetAllAdjacentIntegerPoints(point);

                foreach (var p in adjacentPoints)
                {
                    if (roomIntegerMap[p] == 1)
                    {
                        points.RemoveAt(i);
                        break;
                    }
                }
            }
        }
    }
}