﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.Controllers.RoomGeneration
{
    public class RoomRegion
    {
        public List<Vector2Int> RegionTiles;
        public int TileType;

        public readonly Vector2Int RegionBoundsCenter;
        public readonly List<Vector2Int> RegionPerimeterTiles;
        public RoomRegion NearestSimilarRegion = null;

        private RoomMap parentRoom;

        public RoomRegion(RoomMap roomMap, List<Vector2Int> regionTiles, int type, IntegerMap intMap)
        {
            parentRoom = roomMap;
            RegionTiles = regionTiles;
            TileType = type;

            RegionBoundsCenter = GetCenterTile();
            RegionPerimeterTiles = CalculateFloorRegionPerimeter(intMap);
        }

        private Vector2Int GetCenterTile()
        {
            var centerX = RegionTiles.Sum(item => item.x) / RegionTiles.Count;
            var centerY = RegionTiles.Sum(item => item.y) / RegionTiles.Count;
            return new Vector2Int(centerX, centerY);
        }

        private List<Vector2Int> CalculateFloorRegionPerimeter(IntegerMap intMap)
        {
            if (TileType == 1) return null;
            
            List<Vector2Int> perimeterTiles = new List<Vector2Int>();
            var oppositeTileType = 1;

            foreach (var tile in RegionTiles)
            {
                
                for (int x = tile.x - 1; x <= tile.x + 1; x++)
                for (int y = tile.y - 1; y <= tile.y + 1; y++)
                {
                    if ((x == tile.x || y == tile.y) && intMap[x, y] == oppositeTileType)
                    {
                        perimeterTiles.Add(tile);
                        x += 10;
                        y += 10;
                    }
                }
            }

            return perimeterTiles;
        }

        public Vector2Int GetNearestPerimeterTileToPoint(Vector2Int point)
        {
            var shortestDistance = float.MaxValue;
            Vector2Int closestTile = Vector2Int.zero;

            foreach (var tile in RegionPerimeterTiles)
            {
                if (Vector2Int.Distance(tile, point) < shortestDistance)
                {
                    shortestDistance = Vector2Int.Distance(tile, point);
                    closestTile = tile;
                }
            }

            return closestTile;
        }

        public void ConnectToNearestRegion(IntegerMap intMap)
        {
            if (NearestSimilarRegion == null) return;

            var closestPerimeterThisRegion = GetNearestPerimeterTileToPoint(NearestSimilarRegion.RegionBoundsCenter);
            var closestPerimeterOtherRegion = NearestSimilarRegion.GetNearestPerimeterTileToPoint(closestPerimeterThisRegion);

            var direction = closestPerimeterOtherRegion - closestPerimeterThisRegion;
            var intDistance = Mathf.FloorToInt(direction.magnitude);
            
            for (int i = 1; i <= intDistance; i++)
            {
                var dirNormalized = new Vector2(direction.x / direction.magnitude, direction.y / direction.magnitude);
                var curPointInt = Vector2Int.RoundToInt(closestPerimeterThisRegion + dirNormalized * i);

                for (int x = curPointInt.x - 1; x <= curPointInt.x + 1; x++)
                for (int y = curPointInt.y - 1; y <= curPointInt.y + 1; y++)
                {
                    intMap[x, y] = 0;
                }

                
                //var worldPoint = parentRoom.GetCellPositionWorld(curPointInt, tilemapRef);
                //Debug.DrawRay(worldPoint, Vector3.down * .2f, Color.green, 3f);
            }
        }

        public void CalculateClosestRegion(List<RoomRegion> allRegionsOfType)
        {
            var nearestRegion = allRegionsOfType.FirstOrDefault(item => item != this);
            if (nearestRegion == null) return;

            var closest = Vector2Int.Distance(nearestRegion.RegionBoundsCenter, this.RegionBoundsCenter);
            
            foreach (var otherRegion in allRegionsOfType)
            {
                if (otherRegion != this && 
                    otherRegion.NearestSimilarRegion != this && 
                    Vector2Int.Distance(RegionBoundsCenter, otherRegion.RegionBoundsCenter) < closest)
                {
                    closest = Vector2Int.Distance(RegionBoundsCenter, otherRegion.RegionBoundsCenter);
                    nearestRegion = otherRegion;
                }
            }

            NearestSimilarRegion = nearestRegion;
        }
    }
}