using System;
using UnityEngine;

namespace Code.Controllers.RoomGeneration
{
    public class SceneSwitchComponent : MonoBehaviour
    {
        private BoxCollider2D switchCollider;
        
        private void Awake()
        {
            switchCollider = GetComponent<BoxCollider2D>();
        }

        public void SetColliderTriggerState(bool state)
        {
            switchCollider.isTrigger = state;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("PlayerCharacter"))
            {
                var switchDirection = name switch
                {
                    "Down" => RoomMap.ConnectionDirection.Up,
                    "Up" => RoomMap.ConnectionDirection.Down,
                    "Left" => RoomMap.ConnectionDirection.Right,
                    "Right" => RoomMap.ConnectionDirection.Left,
                    _ => throw new ArgumentOutOfRangeException()
                };
                GlobalGameState.Instance.PlayerCharacterRef.NewRoomSpawnAnchorDirection = switchDirection;
            
                GlobalGameState.Instance.PerformNewSceneLoad();
            }
        }
    }
}