using System.Collections.Generic;
using Code.Data;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Code.Controllers.RoomGeneration
{
    public class RoomMobSpawner
    {
        private BiomeObject thisBiomeObject;
        private int totalMobsSpawnedThisRoom;
        private int totalMobsKilledThisRoom;
        private RoomMap thisRoom;
        private Tilemap thisRoomFloorTilemap;
        private List<SceneSwitchComponent> roomSceneSwitches;
        
        public void InitMobSpawningInRoom(RoomMap roomMap, Tilemap floorTilemap, BiomeObject biomeObject, List<SceneSwitchComponent> sceneSwitches)
        {
            thisBiomeObject = biomeObject;
            thisRoom = roomMap;
            thisRoomFloorTilemap = floorTilemap;
            roomSceneSwitches = sceneSwitches;
            
            var spawnPos = roomMap.GetRandomFloorCells(thisBiomeObject.MaxMobsInitial, floorTilemap);
            SpawnNewMobsAtPositions(spawnPos);
            
            GlobalGameState.Instance.OnMobKilled += RespawnMobs;
        }

        private void RespawnMobs()
        {
            totalMobsKilledThisRoom++;
            
            if (totalMobsSpawnedThisRoom < thisBiomeObject.TotalMobsToKillBeforeUnlockPassage)
            {
                var amountToSpawnMax = thisBiomeObject.TotalMobsToKillBeforeUnlockPassage - totalMobsSpawnedThisRoom;
                var positions = thisRoom.GetRandomFloorCells(Random.Range(1, amountToSpawnMax), thisRoomFloorTilemap);
                SpawnNewMobsAtPositions(positions);
            }
            else if (totalMobsKilledThisRoom >= thisBiomeObject.TotalMobsToKillBeforeUnlockPassage)
            {
                foreach (var sceneSwitch in roomSceneSwitches)
                {
                    sceneSwitch.SetColliderTriggerState(true);
                }
            }
        }
        
        private void SpawnNewMobsAtPositions(List<Vector3> spawnPos)
        {
            foreach (var position in spawnPos)
            {
                var mobTypesAvailable = thisBiomeObject.BiomeSpawnableMobs.Count - 1;
                var mobObject = Object.Instantiate(thisBiomeObject.BiomeSpawnableMobs[Random.Range(0, mobTypesAvailable)],
                    position,
                    Quaternion.identity,
                    GlobalGameState.Instance.MobGroupingAnchor.transform);
                GlobalGameState.Instance.ActiveMobs.Add(mobObject);
                totalMobsSpawnedThisRoom++;
            }
        }
    }
}