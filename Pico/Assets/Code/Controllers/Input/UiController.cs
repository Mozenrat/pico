using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    [SerializeField] private Image HealthBarFill;
    [SerializeField] private TMP_Text HealthCurrent;

    private PlayerCharacterComponent playerRef;
    
    private void Start()
    {
        playerRef = GlobalGameState.Instance.PlayerCharacterRef;
        
        OnPlayerHpChanged(playerRef.CurrentHealth);
    }

    private void OnPlayerHpChanged(int newAmount)
    {
        HealthCurrent.SetText("{0}", newAmount);
        HealthBarFill.fillAmount = Mathf.InverseLerp(0, playerRef.MaxHealth, playerRef.CurrentHealth);
    }

    private void OnEnable()
    {
        GlobalGameState.Instance.PlayerCharacterRef.OnPlayerCurrentHpChanged += OnPlayerHpChanged;
    }

    private void OnDisable()
    {
        GlobalGameState.Instance.PlayerCharacterRef.OnPlayerCurrentHpChanged -= OnPlayerHpChanged;
    }
}