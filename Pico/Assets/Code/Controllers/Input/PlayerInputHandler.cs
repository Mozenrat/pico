using System;
using UnityEngine;

namespace Code
{
    public class PlayerInputHandler : MonoBehaviour
    {
        public class PlayerInputEventArgs : EventArgs
        {
            public Vector3 currentMoveDelta;
            public Vector3 currentCursorPos;
            public bool LmbHeld;
        }

        private Camera mainCam;
        
        public event EventHandler<PlayerInputEventArgs> OnPlayerInput;

        private PlayerInputEventArgs playerInputEventArgs;

        private void Awake()
        {
            mainCam = Camera.main;
            playerInputEventArgs = new PlayerInputEventArgs();
        }

        private void Update()
        {
            var currentMoveDelta = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0f);
            var currentCursorPos = mainCam.ScreenToWorldPoint(Input.mousePosition);
            playerInputEventArgs.currentMoveDelta = currentMoveDelta.normalized;
            playerInputEventArgs.currentCursorPos = currentCursorPos;
            playerInputEventArgs.LmbHeld = Input.GetMouseButton(0);

            OnPlayerInput?.Invoke(this, playerInputEventArgs);
        }
    }
}