using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalGameState : MonoBehaviour
{
    public static GlobalGameState Instance;

    public event Action OnBeforeRoomChange;
    public event Action OnMobKilled;
    
    [HideInInspector] public GameObject MobGroupingAnchor;

    public GameObject MobDeathFlashPrefab;
    public GameObject MobDeathDustPrefab;
    [SerializeField] private GameObject LoadingUIScreen;

    public PlayerCharacterComponent PlayerCharacterRef;
    public List<MobAIComponent> ActiveMobs;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(gameObject);
        
        PlayerCharacterRef = FindObjectOfType<PlayerCharacterComponent>();
    }

    private void Start()
    {
        MobGroupingAnchor = new GameObject("MobAnchor");
        ActiveMobs = new List<MobAIComponent>();
        
        //TODO: its here just for the editor scenes
        OnBeforeRoomChange += () =>
        {
            foreach (var mob in ActiveMobs)
            {
                Destroy(mob.gameObject);
            }
            ActiveMobs.Clear();
            OnMobKilled = null;
        };
        
        AstarPath.active.Scan();
        PerformNewSceneLoad();
    }

    public void KillActiveMob(MobAIComponent mobAIComponent)
    {
        ActiveMobs.Remove(mobAIComponent);
        OnMobKilled?.Invoke();
    }

    public Vector3 GetCurrentPlayerPosition()
    {
        return PlayerCharacterRef.transform.position;
    }

    public void PerformNewSceneLoad()
    {
        ShowLoadingScreen(true);
        
        if (SceneManager.GetSceneByName("RoomScene").isLoaded)
        {
            var unloadHandle = SceneManager.UnloadSceneAsync("RoomScene");
            unloadHandle.completed += operation =>
            {
                OnBeforeRoomChange?.Invoke();
                LoadNewRoomScene();
            };
        }
        else
        {
            OnBeforeRoomChange?.Invoke();
            LoadNewRoomScene();
        }
    }
    
    private void LoadNewRoomScene()
    {
        var handle = SceneManager.LoadSceneAsync("RoomScene", LoadSceneMode.Additive);
        handle.completed += operation =>
        {
            ShowLoadingScreen(false);
        };
    }
    
    private void ShowLoadingScreen(bool state)
    {
        LoadingUIScreen.SetActive(state);
    }
}