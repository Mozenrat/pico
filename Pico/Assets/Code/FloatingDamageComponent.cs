using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloatingDamageComponent : MonoBehaviour
{
    private TMP_Text damageText;

    private Color32 enemyDmgColor = new Color32(255, 108, 0, 255);
    private Color32 playerDmgColor = new Color32(255, 0, 26, 255);

    private void Awake()
    {
        damageText = GetComponent<TMP_Text>();
    }

    public void InitFloatingDamage(int damageValue, bool isEnemyDamaged)
    {
        damageText.SetText("{0}", damageValue);
        damageText.color = isEnemyDamaged ? enemyDmgColor : playerDmgColor;
        transform.localScale = Vector3.one;
        damageText.alpha = 1f;

        StartCoroutine(AnimateDamage());
    }

    private IEnumerator AnimateDamage()
    {
        var toTopTimer = .5f;
        while (toTopTimer > 0)
        {
            toTopTimer -= Time.deltaTime;

            transform.position += new Vector3(0, .001f, 0f);
            yield return null;
        }

        var toBottomTimer = 1f;
        while (toBottomTimer > 0)
        {
            toBottomTimer -= Time.deltaTime;

            transform.localScale *= 0.999f;
            transform.position -= new Vector3(0, .0005f, 0f);
            damageText.alpha -= 0.01f;
            yield return null;
        }

        ProjectileController.Instance.ReturnDamageNumberToPool(this);
    }
}
