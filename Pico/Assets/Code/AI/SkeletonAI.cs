using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.AI.StateMachine;
using Code.Data;
using Pathfinding;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class SkeletonAI : MobAIComponent
{
    public override bool CanAttack
    {
        get
        {
            var playerPosition = GlobalGameState.Instance.GetCurrentPlayerPosition();
            var currentSelfPosition = transform.position;
            var playerDirection = (playerPosition - currentSelfPosition).normalized;
            var rayHit = Physics2D.Raycast(currentSelfPosition,
                playerDirection,
                MobDataObject.MobAttackRange,
                DataValues.LayerMaskPlayerAndWallsLayer);

            if (rayHit &&
                rayHit.collider.gameObject.layer == DataValues.LayerMaskPlayerLayer &&
                attackTimerCurrent >= thisSkeletonAttackCooldown)
            {
                attackTimerCurrent = 0f;
                return true;
            }
            return false;
        }
    }

    private StateMachine stateMachine;
    private MobVisualAndStateHandler stateHandler;

    private int mobHealth;
    
    private bool attackInProgress;
    private bool gettingDamaged;
    private float attackTimerCurrent;

    private float thisSkeletonAttackCooldown;

    private void Awake()
    {
        InitStateMachine();
        stateHandler = GetComponent<MobVisualAndStateHandler>();
        mobHealth = MobDataObject.MobMaxHealth;
        thisSkeletonAttackCooldown = MobDataObject.MobAttackCooldown + Random.Range(.05f, .5f);
    }

    private void InitStateMachine()
    {
        var states = new Dictionary<Type, BaseState>
        {
            { typeof(MeleeMobSpawnState), new MeleeMobSpawnState(this) },
            { typeof(MeleeMobMoveState), new MeleeMobMoveState(this) },
            { typeof(MeleeMobAttackingState), new MeleeMobAttackingState(this) }
        };
        stateMachine = new StateMachine(states);
    }

    private void FixedUpdate()
    {
        attackTimerCurrent += Time.fixedDeltaTime;
        
        if (!gettingDamaged) stateMachine.UpdateStates();
    }

    public override void TakeDamage(int damageValue, Vector2 attackedFromDirection)
    {
        mobHealth -= damageValue;
        var dmgNumPos = (Vector2) transform.position + Random.insideUnitCircle * .25f; //TODO: make damage number spawner responsible for pos calc
        ProjectileController.Instance.SpawnDamageNumberAtPosition(damageValue, dmgNumPos, true);

        if (mobHealth <= 0)
        {
            Instantiate(GlobalGameState.Instance.MobDeathFlashPrefab, transform.position, Quaternion.identity);
        }

        gettingDamaged = true;

        stateHandler.HandleTakeDamageState();
        
        StartCoroutine(GetDamaged(attackedFromDirection, mobHealth <= 0));
    }

    public override void HandleAttack()
    {
        if (attackInProgress) return;
        attackInProgress = true;
        
        stateHandler.HandleAttackingState();

        var playerPosition = GlobalGameState.Instance.GetCurrentPlayerPosition();
        var directionToPlayer = (playerPosition - transform.position).normalized;
        var projSpawnPos = new []
        {
            directionToPlayer,
            directionToPlayer + Vector3.Cross(Vector3.forward, directionToPlayer / Random.Range(4f, 10f)),
            directionToPlayer + Vector3.Cross(Vector3.back, directionToPlayer / Random.Range(4f, 10f))
        };

        StartCoroutine(ShootInPattern(projSpawnPos));
    }
    
    public override void HandleMoveInDirection(Vector2 moveDirection)
    {
        var velocityChange = moveDirection.normalized * (MobDataObject.MobMoveSpeed * Time.deltaTime);
        stateHandler.HandleMovementToPoint(velocityChange, moveDirection);
    }

    private IEnumerator GetDamaged(Vector2 fromDirection, bool dying)
    {
        var knockbackForce = dying ? 
            fromDirection * (MobDataObject.MobKnockbackAmount * 2) : 
            fromDirection * MobDataObject.MobKnockbackAmount;
        stateHandler.HandleKnockback(knockbackForce);
        
        yield return new WaitForSeconds(0.1f);
        
        stateHandler.ReturnSpriteColorToNormal();

        var knockbackTimerCurrent = MobDataObject.MobDamagedAnimationDuration;
        while (knockbackTimerCurrent > 0)
        {
            knockbackTimerCurrent -= Time.deltaTime;
            yield return null;
        }

        if (dying)
        {
            Instantiate(GlobalGameState.Instance.MobDeathDustPrefab, transform.position, quaternion.identity);
            GlobalGameState.Instance.KillActiveMob(this);
            Destroy(gameObject);
        }
        else
        {
            stateHandler.HandleStopTakingDamageState();
            gettingDamaged = false;
        }
    }

    private IEnumerator ShootInPattern(Vector3[] spawnPoints)
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            var proj = ProjectileController.Instance.SpawnProjectileAtPosition(
                ProjectileType.SkeletonBone,
                transform.position + spawnPoints[i],
                quaternion.identity);
            
            proj.InitProjectile(spawnPoints[i], 
                MobDataObject.MobProjectileSpeed, 
                MobDataObject.MobProjectileAcceleration,
                MobDataObject.MobProjectileDamage);
            
            yield return new WaitForSeconds(MobDataObject.MobProjectileSpreadLag);
        }

        yield return new WaitForSeconds(MobDataObject.MobAttackAnimationDuration);

        attackInProgress = false;
    }
}