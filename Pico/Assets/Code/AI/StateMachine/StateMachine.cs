using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.AI.StateMachine;
using UnityEngine;

public class StateMachine
{
    private Dictionary<Type, BaseState> availableStates;

    public BaseState CurrentState { get; private set; }

    public event Action<BaseState> OnAIStateChanged; 

    public StateMachine (Dictionary<Type, BaseState> states)
    {
        availableStates = states;
    }

    public void UpdateStates()
    {
        if (CurrentState == null)
        {
            CurrentState = availableStates.Values.First();
        }

        var nextState = CurrentState?.Tick();

        if (nextState != null && nextState != CurrentState?.GetType())
        {
            SwitchToNextState(nextState);
        }
    }

    private void SwitchToNextState(Type nextState)
    {
        CurrentState = availableStates[nextState];
        OnAIStateChanged?.Invoke(CurrentState);
    }
}