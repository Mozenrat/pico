using System;
using Pathfinding;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.AI.StateMachine
{
    public class MeleeMobMoveState : BaseState
    {
        private MobAIComponent mobAIComponent;

        private bool NeedMoveTarget 
        {
            get
            {
                if (currentPath == null || reachedEndOfPath)
                {
                    return true;
                }

                return false;
            }
        }
    
        private Path currentPath;
        private float nextWaypointDistance = .2f;
        private int currentWaypointIndex = 0;
        private bool reachedEndOfPath = true;
    
        public MeleeMobMoveState(MobAIComponent mobAI) : base(mobAI.gameObject)
        {
            mobAIComponent = mobAI;
        }

        public override Type Tick()
        {
            if (mobAIComponent.CanAttack)
            {
                currentPath = null;
                return typeof(MeleeMobAttackingState);
            }

            if (NeedMoveTarget)
            {
                CalcMoveTarget();
            }
        
            PerformMovement();

            return null;
        }
    
        private void CalcMoveTarget()
        {
            if (AstarPath.active.graphs.Length == 0) return; // Dont search paths right after room generation

            var playerPos = GlobalGameState.Instance.GetCurrentPlayerPosition();
            var randomDirection = (Random.insideUnitCircle * playerPos).normalized;
            var randomDistance = Random.Range(mobAIComponent.MobDataObject.MobEngageDistance/2, mobAIComponent.MobDataObject.MobEngageDistance);
            var circleAroundPlayer = (Vector2)playerPos + randomDirection * randomDistance;
        
            mobPathSeeker.StartPath(transform.position, circleAroundPlayer, OnPathRequestComplete);
            reachedEndOfPath = false;
        }

        private void OnPathRequestComplete(Path p)
        {
            if (!p.error)
            {
                currentPath = p;
                currentWaypointIndex = 0;
            }
            else
            {
                Debug.Log(p.errorLog);
            }
        }
    
        private void PerformMovement()
        {
            if (currentPath != null)
            {
                if (currentWaypointIndex >= currentPath.vectorPath.Count)
                {
                    reachedEndOfPath = true;
                    return;
                }
            
                mobAIComponent.HandleMoveInDirection(currentPath.vectorPath[currentWaypointIndex] - transform.position);
            
                var distNextWp = Vector2.Distance(currentPath.vectorPath[currentWaypointIndex], transform.position);
                if (distNextWp <= nextWaypointDistance)
                {
                    currentWaypointIndex++;
                }
            }
        }
    }
}