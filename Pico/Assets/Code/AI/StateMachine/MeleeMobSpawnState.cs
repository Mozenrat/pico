﻿using System;
using UnityEngine;

namespace Code.AI.StateMachine
{
    public class MeleeMobSpawnState : BaseState
    {
        private readonly MobAIComponent mobAIComponent;
        private readonly Animator mobAnimator;

        public MeleeMobSpawnState(MobAIComponent mobAIComponent) : base(mobAIComponent.gameObject)
        {
            this.mobAIComponent = mobAIComponent;
            mobAnimator = mobAIComponent.GetComponent<Animator>();
        }

        public override Type Tick()
        {
            var wtf = mobAnimator.GetCurrentAnimatorStateInfo(0);
            if (wtf.IsName(mobAIComponent.MobDataObject.MobSpawnStateName))
            {
                return null;
            }

            return typeof(MeleeMobMoveState);
        }
    }
}