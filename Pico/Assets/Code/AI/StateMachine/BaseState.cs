using System;
using Pathfinding;
using UnityEngine;

namespace Code.AI.StateMachine
{
    public abstract class BaseState
    {
        protected BaseState(GameObject go)
        {
            gameObject = go;
            transform = go.transform;
            mobPathSeeker = go.GetComponent<Seeker>();
        }
        protected GameObject gameObject;
        protected Transform transform;
        protected Seeker mobPathSeeker;
        public abstract Type Tick();
    }
}