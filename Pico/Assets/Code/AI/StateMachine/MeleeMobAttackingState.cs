using System;
using UnityEngine;

namespace Code.AI.StateMachine
{
    public class MeleeMobAttackingState : BaseState
    {
        private MobAIComponent mobAIComponent;

        private float attackTimerCurrent;

        public MeleeMobAttackingState(MobAIComponent mobAI) : base(mobAI.gameObject)
        {
            mobAIComponent = mobAI;
        }

        public override Type Tick()
        {
            attackTimerCurrent += Time.fixedDeltaTime;
            mobAIComponent.HandleAttack();

            if (attackTimerCurrent < mobAIComponent.MobDataObject.MobAttackAnimationDuration)
            {
                return null;
            }

            attackTimerCurrent = 0f;
            return typeof(MeleeMobMoveState);
        }
    }
}