using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.Data;
using UnityEngine;

public class MobVisualAndStateHandler : MonoBehaviour
{
    private Animator mobAnimator;
    private Rigidbody2D mobRigidbody;
    private SpriteRenderer mobSpriteRenderer;
    private MaterialPropertyBlock materialPropertyBlock;

    private Transform playerPosition;
    
    private void Awake()
    {
        mobAnimator = GetComponent<Animator>();
        mobRigidbody = GetComponent<Rigidbody2D>();
        mobSpriteRenderer = GetComponentsInChildren<SpriteRenderer>().Single(item => item.gameObject.name == "Mob_Sprite");
        materialPropertyBlock = new MaterialPropertyBlock();
        
        playerPosition = GlobalGameState.Instance.PlayerCharacterRef.transform;
    }

    public void HandleAttackingState()
    {
        FaceSpriteToPoint(playerPosition.position);
        mobAnimator.SetTrigger(DataValues.Animator_MobAttack);
        mobAnimator.SetFloat(DataValues.Animator_MobSpeed, 0f);
        mobRigidbody.velocity = Vector2.zero;
    }

    public void HandleMovementToPoint(Vector2 velocityChange, Vector2 moveDirection)
    {
        mobRigidbody.velocity = velocityChange;
            
        mobAnimator.SetFloat(DataValues.Animator_MobSpeed, moveDirection.magnitude);
        FaceSpriteToPoint(mobRigidbody.position + moveDirection);
    }

    public void HandleTakeDamageState()
    {
        mobSpriteRenderer.GetPropertyBlock(materialPropertyBlock);
        materialPropertyBlock.SetColor(DataValues.Shader_Tint_Property, Color.white);
        mobSpriteRenderer.SetPropertyBlock(materialPropertyBlock);
        mobAnimator.SetFloat(DataValues.Animator_MobSpeed, 0f);
        mobAnimator.SetBool(DataValues.Animator_MobDamaged, true);
        mobRigidbody.velocity = Vector2.zero;
        
        FaceSpriteToPoint(playerPosition.position);
    }

    public void HandleStopTakingDamageState()
    {
        mobAnimator.SetBool(DataValues.Animator_MobDamaged, false);
    }

    public void ReturnSpriteColorToNormal()
    {
        mobSpriteRenderer.GetPropertyBlock(materialPropertyBlock);
        materialPropertyBlock.SetColor(DataValues.Shader_Tint_Property, Color.black);
        mobSpriteRenderer.SetPropertyBlock(materialPropertyBlock);
    }

    public void HandleKnockback(Vector2 knockbackForce)
    {
        mobRigidbody.AddForce(knockbackForce, ForceMode2D.Impulse);
    }

    private void FaceSpriteToPoint(Vector3 targetPoint)
    {
        var currentX = transform.position.x;
        if (targetPoint.x > currentX)
        {
            mobSpriteRenderer.flipX = false;
        }
        else if (targetPoint.x < currentX)
        {
            mobSpriteRenderer.flipX = true;
        }
    }
}