using System.Collections;
using System.Collections.Generic;
using Code.Data.Mob;
using UnityEngine;

[RequireComponent(typeof(MobVisualAndStateHandler))]
public abstract class MobAIComponent : MonoBehaviour
{
    public MobDataObject MobDataObject;
    public abstract bool CanAttack { get; }
    public abstract void HandleAttack();
    public abstract void HandleMoveInDirection(Vector2 moveDirection);
    public abstract void TakeDamage(int damageValue, Vector2 attackedFromDirection);
}