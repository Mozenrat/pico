using Code;
using UnityEngine;

public class WeaponAttackHandler : MonoBehaviour
{
    private PlayerInputHandler playerInputHandler;
    private SpriteRenderer weaponSpriteRenderer;
    private Transform parentTransform;
    
    [SerializeField] private GameObject weaponArcAnim;
    [SerializeField] private PlayerProjectile PlayerProjectilePrefab;
    private bool arcAnimating = false;
    private GameObject currentArcObj;

    private float weaponOffset = -.4f;
    private bool weaponSwingInProgress;
    private Vector3 weaponSwingAxis = Vector3.forward;

    private float swingAnimationTimeCurrent;
    private float swingAnimationTimer = 0.15f;

    private float swingCooldownTimeCurrent;
    private float swingCooldownTimer = 0.3f;

    private void Awake()
    {
        playerInputHandler = FindObjectOfType<PlayerInputHandler>();
        weaponSpriteRenderer = GetComponent<SpriteRenderer>();
        parentTransform = transform.parent;
    }

    private void HandleAttackInput(object sender, PlayerInputHandler.PlayerInputEventArgs eventArgs)
    {
        var weaponHolderPosition = parentTransform.position;
        var cursorDirection = eventArgs.currentCursorPos - weaponHolderPosition;
        cursorDirection.z = 0f;
        
        swingCooldownTimeCurrent += Time.deltaTime;
        
        if ((weaponSwingInProgress || eventArgs.LmbHeld) && swingCooldownTimeCurrent >= swingCooldownTimer)
        {
            weaponSwingInProgress = true;
            PerformWeaponSwing();
        }
        else
        {
            transform.position = weaponHolderPosition + (weaponOffset * cursorDirection.normalized);

            var weaponLookAnchor = Vector3.Cross(Vector3.forward, cursorDirection.normalized);
            var rotTarget = Quaternion.LookRotation(Vector3.forward, weaponLookAnchor);
            transform.rotation = rotTarget;
        }

        if (weaponSwingInProgress && !arcAnimating)
        {
            arcAnimating = true;
            currentArcObj = Instantiate(
                weaponArcAnim, 
                weaponHolderPosition + cursorDirection.normalized * 2f, 
                Quaternion.LookRotation(Vector3.forward, cursorDirection));
            
            Instantiate(PlayerProjectilePrefab, 
                weaponHolderPosition + cursorDirection.normalized,
                Quaternion.LookRotation(Vector3.forward, cursorDirection));
        }
    }

    private void PerformWeaponSwing()
    {
        swingAnimationTimeCurrent += Time.deltaTime;
        
        if (swingAnimationTimeCurrent >= swingAnimationTimer)
        {
            var isSpriteFlipped = weaponSpriteRenderer.flipY;
            swingAnimationTimeCurrent = 0f;
            swingCooldownTimeCurrent = 0f;
            weaponSwingInProgress = false;
            
            arcAnimating = false;
            Destroy(currentArcObj);
            currentArcObj = null;
            
            weaponSpriteRenderer.flipY = !isSpriteFlipped;
            weaponSwingAxis = !isSpriteFlipped ? Vector3.back : Vector3.forward;
        }
        else
        {
            var weaponHolderPosition = parentTransform.position;
            transform.RotateAround(weaponHolderPosition, weaponSwingAxis, 2000 * Time.deltaTime);
            transform.Rotate(weaponSwingAxis, 10f);
        }
    }

    private void OnEnable()
    {
        playerInputHandler.OnPlayerInput += HandleAttackInput;
    }

    private void OnDisable()
    {
        playerInputHandler.OnPlayerInput -= HandleAttackInput;
    }
}