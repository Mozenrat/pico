using System;
using System.Collections;
using System.Collections.Generic;
using Code.Data;
using UnityEngine;

public class MobProjectile : MonoBehaviour
{
    [SerializeField] public GameObject ProjectileCollisionEffect;
    [SerializeField] public ProjectileType ProjectileType;

    private Rigidbody2D projRigidBody;

    private Vector2 projDirection;
    private float projSpeed;
    private float projAcceleration;
    private int projDamage;

    private void Awake()
    {
        projRigidBody = GetComponent<Rigidbody2D>();
    }

    public void InitProjectile(Vector2 direction, float speed, float acceleration, int damageValue)
    {
        projDirection = direction;
        projSpeed = speed;
        projAcceleration = acceleration;
        projDamage = damageValue;

        projRigidBody.velocity = projDirection * projSpeed;
    }

    private void FixedUpdate()
    {
        if (projAcceleration != 0f)
        {
            projRigidBody.velocity += projDirection * (projSpeed * projAcceleration * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == DataValues.LayerMaskPlayerLayer)
        {
            GlobalGameState.Instance.PlayerCharacterRef.TakeDamage(projDamage);
        }
        else if (other.gameObject.layer == DataValues.LayerMaskEnvironmentPropsLayer)
        {
            var damageable = other.GetComponent<DamageableEntity>();
            damageable.GetDamaged(projDamage);
        }
        
        Instantiate(ProjectileCollisionEffect, transform.position, Quaternion.identity); //TODO: pool these
        DestroyProjectile();
    }

    private void OnEnable()
    {
        projDirection = Vector2.zero;
        projSpeed = 0f;
    }

    private void DestroyProjectile()
    {
        ProjectileController.Instance.ReturnProjectileToPool(this);
    }
}